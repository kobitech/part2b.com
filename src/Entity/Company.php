<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", length=128, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="vat_number", length=32, nullable=true)
     */
    protected $vat;

    /**
     * @ORM\Column(name="languages", length=128, nullable=true)
     */
    protected $languages;

    /**
     * @ORM\Column(name="group_affiliation", length=128, nullable=true)
     */
    protected $group_affiliation;

    /**
     * @ORM\Column(name="type", length=64, nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="line_of_bussiness", length=128, nullable=true)
     */
    protected $line_of_bussiness;

    /**
     * @ORM\Column(name="founding_year", length=32, nullable=true)
     */
    protected $founding_year;

    /**
     * @ORM\Column(name="systems", type="text", nullable=true)
     */
    protected $systems;

    /**
     * @ORM\Column(name="reference", type="text", nullable=true)
     */
    protected $references;

    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param mixed $vat
     */
    public function setVat($vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages): void
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getGroupAffiliation()
    {
        return $this->group_affiliation;
    }

    /**
     * @param mixed $group_affiliation
     */
    public function setGroupAffiliation($group_affiliation): void
    {
        $this->group_affiliation = $group_affiliation;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLineOfBussiness()
    {
        return $this->line_of_bussiness;
    }

    /**
     * @param mixed $line_of_bussiness
     */
    public function setLineOfBussiness($line_of_bussiness): void
    {
        $this->line_of_bussiness = $line_of_bussiness;
    }

    /**
     * @return mixed
     */
    public function getFoundingYear()
    {
        return $this->founding_year;
    }

    /**
     * @param mixed $founding_year
     */
    public function setFoundingYear($founding_year): void
    {
        $this->founding_year = $founding_year;
    }

    /**
     * @return mixed
     */
    public function getSystems()
    {
        return $this->systems;
    }

    /**
     * @param mixed $systems
     */
    public function setSystems($systems): void
    {
        $this->systems = $systems;
    }

    /**
     * @return mixed
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * @param mixed $references
     */
    public function setReferences($references): void
    {
        $this->references = $references;
    }
}
