<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="area_countries")
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="binary_code", type="string", length=2)
     *
     */
    protected $binaryAreaCode;

    /**
     * @ORM\Column(name="triple_code", type="string", length=3)
     *
     */
    protected $tripleAreaCode;

    /**
     * @ORM\Column(name="name", type="string", length=64)
     *
     */
    protected $name;

    /**
     * @ORM\Column(name="phone_area_code", type="string", length=4)
     *
     */
    protected $phoneAreaCode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBinaryAreaCode()
    {
        return $this->binaryAreaCode;
    }

    /**
     * @param mixed $binaryCode
     */
    public function setBinaryAreaCode($binaryCode): void
    {
        $this->binaryAreaCode = $binaryCode;
    }

    /**
     * @return mixed
     */
    public function getTripleAreaCode()
    {
        return $this->tripleAreaCode;
    }

    /**
     * @param mixed $tripleCode
     */
    public function setTripleAreaCode($tripleCode): void
    {
        $this->tripleAreaCode = $tripleCode;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhoneAreaCode()
    {
        return $this->phoneAreaCode;
    }

    /**
     * @param mixed $phoneAreaCode
     */
    public function setPhoneAreaCode($phoneAreaCode): void
    {
        $this->phoneAreaCode = $phoneAreaCode;
    }

    public function __toString() {
        return $this->name;
    }

    public function toArray() {
        return $this;
    }
}
