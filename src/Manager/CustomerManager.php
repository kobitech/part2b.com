<?php

namespace App\Manager;

use App\Entity\Company;
use App\Entity\CompanyBilling;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

class CustomerManager
{
    const HASH_ENC_KEY = '9154eb2cc4cf88473cb598269f7ae752';

    public $customerRepository;

    public function __construct(CustomerRepository $customerRepository, UserPasswordEncoderInterface $encoder)
    {
        $this->customerRepository = $customerRepository;
        $this->encoder = $encoder;
    }

    public function register($data = array())
    {
        $result = ['code' => -99, 'msg' => 'an error, not proccess'];

        if (isset($data['type']) && in_array($data['type'], ['CUSTOMER', 'SUPPLIER'])) {
            $functionName = 'registerType' . ucfirst(strtolower($data['type']));

            $result = $this->$functionName($data);
        }

        return $result;
    }

    public function forgot($data = array())
    {
        $result = ['code' => -99, 'msg' => 'an error, not proccess'];

        if (isset($data['email']) && !empty($data['email']))
        {
            $customer = $this->customerRepository->findByEmail($data['email']);

            if (!empty($customer)) {
                $date  = strtotime(date('Y-m-d H:i:s', time() + (3 * 7200)));
                $hash = password_hash(base64_encode($data['email'] . $date . self::HASH_ENC_KEY),PASSWORD_BCRYPT);

                $result = ['code' => 1, 'msg' => 'OK', 'data' => ['date' => $date, 'email' => $data['email'], 'hash' => $hash]];
            }
        }

        return $result;
    }

    public function reset($data = array())
    {
        $result = ['code' => -99, 'msg' => 'an error, not proccess'];

        if (isset($data['date']) && isset($data['email']) && isset($data['hash'])) {
            if (time() > $data['date']) {
                return ['code' => -2, 'msg' => 'token süresi dolmuş'];
            }

            $passwordHash = base64_encode($data['email'] . $data['date'] . self::HASH_ENC_KEY);

            if (!password_verify($passwordHash, $data['hash'])) {
                return ['code' => -3, 'msg' => 'token hatalı'];
            }

            $em = $this->customerRepository->getEntityManager();

            $customer = $this->customerRepository->findByEmail($data['email']);

            if (!empty($customer)) {
                $customer->setPassword($this->encoder->encodePassword($customer, $data['password']));
            }

            $em->persist($customer);
            $em->flush($customer);

            return ['code' => 0, 'msg' => 'OK'];
        }

        return $result;
    }

    private function registerTypeCustomer($data) {

        if (isset($data['email']) && isset($data['password']))
        {
            $validator = Validation::createValidator();

            $constraint = new Assert\Collection(
                array(
                    'fields' => array(
                        'additional_params' => new Assert\Collection(
                            array(
                                'fields' => array(
                                    'firstName' => new Assert\Length(array('min' => 3)),
                                    'lastName' => new Assert\Length(array('min' => 3)),
                                    'company' => new Assert\Length(array('min' => 3)),
                                    'job' => new Assert\Length(array('min' => 3)),
                                    'department' => new Assert\Length(array('min' => 2)),
                                    'phone' => new Assert\NotBlank(),
                                    'address' => array(
                                        new Assert\NotBlank(),
                                        new Assert\Length(array('min' => 10))
                                    )
                                ),
                                'allowExtraFields' => true,
                                'allowMissingFields' => true
                            )
                        ),
                        'type' => new Assert\NotBlank(),
                        'email' => new Assert\Email(),
                        'password' => new Assert\Length(array('min' => 5))
                    ),
                    'allowExtraFields' => true,
                    'allowMissingFields' => true,
                )
            );

            $errors = $validator->validate($data, $constraint);

            if (0 === count($errors)) {
                $result = $this->createCustomer($data['type'], $data['email'], $data['password'], $data['additional_params']);

                if (is_object($result)) {
                    return ['code' => 0, 'msg' => 'OK'];
                } else if ($result == 'NONU') {
                    return ['code' => -1, 'msg' => 'Bu e-posta adresi zaten kullanımda.'];
                } else if ($result == 'ERR') {
                    return ['code' => -2, 'msg' => 'Beklenmedik bir hata oluştu.'];
                }
                // ... this IS a valid email address, do something
            } else {
                return ['code' => -3, 'msg' => 'Validasyon hatası.'];
            }
        }

        return ['code' => -98];
    }

    private function registerTypeSupplier($data) {

        if (isset($data['email']) && isset($data['password']))
        {
            $validator = Validation::createValidator();

            $constraint = new Assert\Collection(
                array(
                    'fields' => array(
                        'additional_params' => new Assert\Collection(
                            array(
                                'fields' => array(
                                    'firstName' => new Assert\Length(array('min' => 3)),
                                    'lastName' => new Assert\Length(array('min' => 3)),
                                    'company' => new Assert\Length(array('min' => 3)),
                                    'job' => new Assert\Length(array('min' => 3)),
                                    'department' => new Assert\Length(array('min' => 2)),
                                    'phone' => new Assert\NotBlank(),
                                    'address' => array(
                                        new Assert\NotBlank(),
                                        new Assert\Length(array('min' => 10))
                                    )
                                ),
                                'allowExtraFields' => true,
                                'allowMissingFields' => true
                            )
                        ),
                        'type' => new Assert\NotBlank(),
                        'email' => new Assert\Email(),
                        'password' => new Assert\Length(array('min' => 5))
                    ),
                    'allowExtraFields' => true,
                    'allowMissingFields' => true,
                )
            );


            $companyId = null;
            $em = $this->customerRepository->getEntityManager();

            if (isset($data['company_info']) && !empty($data['company_info']))
            {
                $companyInfo = new Company();

                $companyInfo->setType($data['company_info']['legalForm']);
                $companyInfo->setFoundingYear($data['company_info']['foundingYear']);
                $companyInfo->setLineOfBussiness($data['company_info']['lineOfBusiness']);
                $companyInfo->setGroupAffiliation($data['company_info']['groupAffiliation']);
                $companyInfo->setLanguages(implode(',', $data['company_info']['businessLanguage']));
                $companyInfo->setVat($data['company_info']['vatRegistrationNumber']);
                $companyInfo->setName($data['company_info']['name']);

                $em->persist($companyInfo);
                $em->flush($companyInfo);

                $companyId = $companyInfo->getId();
            }

            if (isset($data['primary_banking_account']) && !empty($data['primary_banking_account']) && !empty($companyId))
            {
                $em = $this->customerRepository->getEntityManager();

                $companyBilling = new CompanyBilling();

                $companyBilling->setCompany($em->getReference('App:Company', $companyId));
                $companyBilling->setIban($data['primary_banking_account']['iban']);
                $companyBilling->setBankName($data['primary_banking_account']['bank_name']);
                $companyBilling->setSwiftCode($data['primary_banking_account']['swift_code']);
                $companyBilling->setBic($data['primary_banking_account']['bic']);

                $em->persist($companyBilling);
                $em->flush($companyBilling);
            }

            if (isset($data['systems']) && !empty($data['systems']) && !empty($companyId))
            {
                $system['erp'] = implode(',', $data['systems']['erp']);

                if ($data['systems']['otherErp']) {
                    $system['erp'] .= !empty($system['erp']) ? ',' : '' . $data['systems']['otherErp'];
                }


                $system['cad3D'] = implode(',', $data['systems']['cad3D']);

                if ($data['systems']['otherCad3D']) {
                    $system['cad3D'] .= !empty($system['cad3D']) ? ',' : '' . $data['systems']['cad3D'];
                }


                $system['cad2D'] = implode(',', $data['systems']['cad2D']);

                if ($data['systems']['otherCad2D']) {
                    $system['cad2D'] .= !empty($system['cad2D']) ? ',' : '' . $data['systems']['cad2D'];
                }

                $system['elektroCAD'] = implode(',', $data['systems']['elektroCAD']);

                if ($data['systems']['otherElektroCAD']) {
                    $system['elektroCAD'] .= !empty($system['elektroCAD']) ? ',' : '' . $data['systems']['otherElektroCAD'];
                }

                $system['cadInterchangeFormats'] = implode(',', $data['systems']['cadInterchangeFormats']);

                if ($data['systems']['otherCadInterchangeFormats']) {
                    $system['cadInterchangeFormats'] .= !empty($system['otherCadInterchangeFormats']) ? ',' : '' . $data['systems']['otherCadInterchangeFormats'];
                }

                $companyInfo->setSystems(json_encode($system));


            }

            if (isset($data['references']) && !empty($data['references']) && !empty($companyId)) {
                $companyInfo->setReferences(json_encode($data['references']));
            }

            $em->persist($companyInfo);
            $em->flush($companyInfo);


            $result = $this->createCustomer($data['type'], $data['email'], $data['password'], $data['additional_params']);

            if (is_object($result)) {
                return ['code' => 0, 'msg' => 'OK'];
            } else if ($result == 'NONU') {
                return ['code' => -1, 'msg' => 'Bu e-posta adresi zaten kullanımda.'];
            } else if ($result == 'ERR') {
                return ['code' => -2, 'msg' => 'Beklenmedik bir hata oluştu.'];
            }

        }

        return ['code' => -98];

    }

    private function createCustomer($type, $email, $password, $additionalParams)
    {
        $em = $this->customerRepository->getEntityManager();

        $customer = new Customer();

        try {
            $customer->setType($type);
            $customer->setEmail($email);
            $customer->setPassword($this->encoder->encodePassword($customer, $password));


            if (isset($additionalParams['firstName']) && !empty($additionalParams['firstName'])) $customer->setFirstname($additionalParams['firstName']);
            if (isset($additionalParams['lastName']) && !empty($additionalParams['lastName'])) $customer->setLastname($additionalParams['lastName']);
            if (isset($additionalParams['company']) && !empty($additionalParams['company'])) $customer->setCompanyName($additionalParams['company']);
            if (isset($additionalParams['job']) && !empty($additionalParams['job'])) $customer->setJob($additionalParams['job']);
            if (isset($additionalParams['phone']) && !empty($additionalParams['phone'])) $customer->setPhone($additionalParams['phone']);
            if (isset($additionalParams['fax']) && !empty($additionalParams['fax'])) $customer->setFax($additionalParams['fax']);
            if (isset($additionalParams['address']) && !empty($additionalParams['address'])) $customer->setAddress($additionalParams['address']);
            if (isset($additionalParams['streetNo']) && !empty($additionalParams['streetNo'])) $customer->setStreetNo($additionalParams['streetNo']);
            if (isset($additionalParams['postalCode']) && !empty($additionalParams['postalCode'])) $customer->setPostalCode($additionalParams['postalCode']);

            if (isset($additionalParams['country']) && !empty($additionalParams['country'])) {
                $customer->setCountry($em->getReference('App:Country', $additionalParams['country']['id']));
                $customer->setCountryName($additionalParams['country']['name']);
            }

            if (isset($additionalParams['city']) && !empty($additionalParams['city'])) {
                if (!empty($additionalParams['city']['id'])) {
                    $customer->setCity($em->getReference('App:City', $additionalParams['city']['id']));
                }

                $customer->setCityName($additionalParams['city']['name']);
            }

            $em->persist($customer);
            $em->flush($customer);

        } catch (UniqueConstraintViolationException $e) {
            return 'NONU';
        } catch (Exception $e) {
            echo $e->getMessage();
            return 'ERR';
        }

        return $customer;
    }

    public function isEmailExists($email) {
        $user = $this->customerRepository->findByEmail($email);

        return !empty($user);
    }
}