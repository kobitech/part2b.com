<?php

namespace App\Repository;

use App\Entity\CompanyBilling;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompanyBilling|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyBilling|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyBilling[]    findAll()
 * @method CompanyBilling[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyBillingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompanyBilling::class);
    }

//    /**
//     * @return CompanyBilling[] Returns an array of CompanyBilling objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyBilling
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
