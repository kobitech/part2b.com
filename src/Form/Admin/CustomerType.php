<?php

namespace App\Form\Admin;

use Doctrine\DBAL\Types\ArrayType;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['disabled' => true])
            ->add('firstName')
            ->add('lastName')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Bayi' => 'SUPPLIER',
                    'Kullanıcı' => 'CUSTOMER'
                ]
            ])
            ->add('save', SubmitType::class)
        ;
    }
}