<?php

namespace App\Form;

use App\Entity\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('binaryAreaCode', null, [
                    'label' => 'Ülke Kodu'
            ])
            ->add('tripleAreaCode', null, [
                    'label' => 'Üç Harf Ülke Kodu'
                ])
            ->add('name', null, [
                'label' => 'Ad'
            ])
            ->add('phoneAreaCode', null, [
                'label' => 'Telefon Kodu'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Country::class,
        ]);
    }
}
