<?php

namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class SecurityController extends AbstractApiController
{
    /**
     * @Route("/api/login", name="api_login")
     */
    public function login()
    {

    }

    /**
     * @Route("/api/register", name="api_register")
     */
    public function register(Request $request)
    {
        $resp = ['code' => -1, 'msg' => 'an error'];

        if ($request->isMethod('post'))
        {
            $data = array_merge($request->query->all(), $request->request->all());

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection(array(
                'email' => new Assert\Email(),
                'type' => new Assert\Choice(array('customer', 'supplier')),
                'password' => new Assert\Length(array('min' => 5, 'max' => 31)),
                'password_repeat' => new Assert\EqualTo(array('propertyPath' => 'password')),
            ));

            $violations = $validator->validate($data, $constraint);

            if ($violations->count() == 0) {
                $resp = ['code' => 0, 'msg' => 'ok'];
            }


        }

        return JsonResponse::create($resp);
    }
}