<?php

namespace App\Controller\Api;

use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AreaController extends AbstractApiController
{
    /**
     * @Route("/api/area/countries", name="api_area_country")
     */
    public function country(CountryRepository $countryRepository)
    {
        $countries = $countryRepository->findAll();

        $res = array();
        foreach ($countries as $country) {
            array_push($res, array('id' => $country->getId(), 'name' => $country->getName()));
        }
        return JsonResponse::create($res);
    }

    /**
     * @Route("/api/area/cities", name="api_area_cities")
     */
    public function city(Request $request, CityRepository $cityRepository)
    {
        $countryId = $request->query->get('country_id', null);

        $res = array();
        if (!empty($countryId)) {
            $cities = $cityRepository->getCitiesByCountryId($countryId);

            foreach ($cities as $city) {
                array_push($res, array('id' => $city->getId(), 'name' => $city->getName()));
            }
        }

        return JsonResponse::create($res);
    }

}