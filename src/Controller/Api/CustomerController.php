<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use App\Manager\CustomerManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends AbstractApiController
{
    /**
     * @Route("/api/register", name="api_customer_register")
     */
    public function register(Request $request, CustomerManager $customerManager)
    {
        $params = $request->getContent() ? $request->getContent() : array();
        $params = !empty($params) ? json_decode($params, true) : array();

        $res = ['code' => -1, 'msg' => 'error'];
        if (!empty($params)) {
            $res = $customerManager->register($params);
        }

        return JsonResponse::create($res);
    }

    /**
     * @Route("/api/forgot_password", name="api_customer_forgot")
     */
    public function forgot(Request $request, CustomerManager $customerManager)
    {
        $params = $request->getContent() ? $request->getContent() : array();
        $params = !empty($params) ? json_decode($params, true) : array();

        $res = ['code' => -1, 'msg' => 'error'];
        if ($request->isMethod('post') && !empty($params)) {
            if (!empty($params)) {
                $res = $customerManager->forgot($params);
            }
        }

        return JsonResponse::create($res);
    }

    /**
     * @Route("/api/reset_password", name="api_customer_reset")
     */
    public function reset(Request $request, CustomerManager $customerManager)
    {
        $params = $request->getContent() ? $request->getContent() : array();
        $params = !empty($params) ? json_decode($params, true) : array();

        $res = ['code' => -1, 'msg' => 'error'];
        if ($request->isMethod('post') && !empty($params)) {
            if (!empty($params)) {
                $res = $customerManager->reset($params);
            }
        }

        return JsonResponse::create($res);
    }

    /**
     * @Route("/api/check_email/{email}", name="api_check_email")
     */
    public function checkEmail($email, CustomerManager $customerManager)
    {
        $response = [
            "exists" => $customerManager->isEmailExists($email)
        ];

        return JsonResponse::create($response);
    }
}