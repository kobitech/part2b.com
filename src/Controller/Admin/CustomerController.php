<?php

namespace App\Controller\Admin;

use App\Entity\Customer;
use App\Form\Admin\CustomerType;
use App\Repository\CustomerRepository;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Column\BoolColumn;
use Omines\DataTablesBundle\Column\MapColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;

/**
 * @Route("/admin/customer")
 */
class CustomerController extends AbstractAdminController
{
    use DataTablesTrait;

    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @Route("/", name="customer", methods={"GET", "POST"})
     * @Route("/index", name="customer_index", methods={"GET", "POST"})
     */
    public function index(Request $request)
    {
        $datatable = $this->createDataTable();

        $datatable->add('id', TextColumn::class, ['label' => '#ID']);
        $datatable->add('type', MapColumn::class, [
            'default' => '-',
            'label' => 'Kullanıcı Tipi',
            'map' => [
                'SUPPLIER' => 'Bayi',
                'CUSTOMER' => 'Kullanıcı',
            ],
            'searchable' => true,
            'globalSearchable' => true
        ]);
        $datatable->add('firstName', TextColumn::class, ['label' => 'Ad']);
        $datatable->add('lastName', TextColumn::class, ['label' => 'Soyad']);
        $datatable->add('email', TextColumn::class, ['label' => 'E-posta']);
        $datatable->add('buttons', TextColumn::class, [
            'label' => '',
            'render' => function($value, $context) {
                return sprintf('<a href="%s" class="btn btn-sm btn-success">Detay</a>', '/admin/customer/edit/' . $context->getId());
            }
        ]);


        $datatable->createAdapter(ORMAdapter::class, [
            'entity' => Customer::class,
            'query' => function (QueryBuilder $builder) {
                $builder
                    ->select('c')
                    ->from(Customer::class, 'c');
            },
        ]);

        $datatable->handleRequest($request);

        if ($datatable->isCallback()) {
            return $datatable->getResponse();
        }

        return $this->render('Admin/customer/index.html.twig', ['datatable' => $datatable]);
    }

    /**
     * @Route("/edit/{id}", name="customer_edit", methods={"GET", "POST"})
     */
    public function edit($id, Request $request)
    {
        $customer = $this->customerRepository->find($id);

        $form = $this->createForm(CustomerType::class, $customer);

        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            $this->addFlash('success', 'Güncelleme işlemi başarılı bir şekilde gerçekleşti.');

            return $this->redirect($this->generateUrl('customer_edit', ['id' => $customer->getId()]));
        }

        return $this->render('Admin/customer/edit.html.twig', ['form' => $form->createView(), 'customer' => $customer]);
    }
}