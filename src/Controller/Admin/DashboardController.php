<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractAdminController
{
    /**
     * @Route("/admin", name="admin_dashboard")
     * @Route("/admin/dashboard", name="admin_dashboard_index")
     */
    public function index() {
        return $this->render('Admin/dashboard/index.html.twig');
    }
}