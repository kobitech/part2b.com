#!/bin/bash

CURRENT_DIR=$DEPLOY_PATH/$APP_NAME/current

RELEASE_NAME=`date +"%Y-%m-%d-%H%M%S"`

CURRENT_RELEASE=$DEPLOY_PATH/$APP_NAME/releases/$RELEASE_NAME



# From local machine, get hash of the head of the desired branch

# Required to checkout the branch - is there a better way to do this?

APP_HASH=`git ls-remote $APP_REPO $BRANCH | awk -F "t" '{print $1}'`



for SERVER in ${DEPLOY_SERVER[@]}

do

echo "Deploying on $SERVER"

ssh -t $DEPLOY_USER@$SERVER "cd $DEPLOY_PATH/$APP_NAME/releases &amp;&amp;

git clone -q $GIT_USER@$APP_REPO $RELEASE_NAME &amp;&amp;

cd $RELEASE_NAME &amp;&amp;

git checkout -q -b deploy $APP_HASH &amp;&amp;

ln -nfs $CURRENT_RELEASE $CURRENT_DIR"

done

echo "Finished successfully"